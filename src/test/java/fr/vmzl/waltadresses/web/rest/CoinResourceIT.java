package fr.vmzl.waltadresses.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.vmzl.waltadresses.WaltAdressesApp;
import fr.vmzl.waltadresses.domain.Coin;
import fr.vmzl.waltadresses.repository.CoinRepository;
import fr.vmzl.waltadresses.service.CoinService;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link CoinResource} REST controller.
 */
@SpringBootTest(classes = WaltAdressesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class CoinResourceIT {
    private static final String DEFAULT_COIN_ID = "AAAAAAAAAA";
    private static final String UPDATED_COIN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SYMBOL = "AAAAAAAAAA";
    private static final String UPDATED_SYMBOL = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_HOMEPAGE = "AAAAAAAAAA";
    private static final String UPDATED_HOMEPAGE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final Integer DEFAULT_COINGECKO_RANK = 1;
    private static final Integer UPDATED_COINGECKO_RANK = 2;

    private static final Double DEFAULT_COINGECKO_SCORE = 1D;
    private static final Double UPDATED_COINGECKO_SCORE = 2D;

    private static final Double DEFAULT_DEVELOPPER_SCORE = 1D;
    private static final Double UPDATED_DEVELOPPER_SCORE = 2D;

    private static final Double DEFAULT_COMMUNITY_SCORE = 1D;
    private static final Double UPDATED_COMMUNITY_SCORE = 2D;

    private static final Double DEFAULT_LIQUIDITY_SCORE = 1D;
    private static final Double UPDATED_LIQUIDITY_SCORE = 2D;

    private static final LocalDate DEFAULT_LAST_UPDATED = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_UPDATED = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_MARKET_CAP = 1;
    private static final Integer UPDATED_MARKET_CAP = 2;

    private static final Integer DEFAULT_MARKET_CAP_RANK = 1;
    private static final Integer UPDATED_MARKET_CAP_RANK = 2;

    private static final Double DEFAULT_HIGH_24_HUSD = 1D;
    private static final Double UPDATED_HIGH_24_HUSD = 2D;

    private static final Double DEFAULT_LOW_24_HUSD = 1D;
    private static final Double UPDATED_LOW_24_HUSD = 2D;

    private static final Double DEFAULT_PRICE_CHANGE_PERCENT_1_H = 1D;
    private static final Double UPDATED_PRICE_CHANGE_PERCENT_1_H = 2D;

    private static final Double DEFAULT_PRICE_CHANGE_PERCENT_24_H = 1D;
    private static final Double UPDATED_PRICE_CHANGE_PERCENT_24_H = 2D;

    private static final Double DEFAULT_PRICE_CHANGE_PERCENT_7_D = 1D;
    private static final Double UPDATED_PRICE_CHANGE_PERCENT_7_D = 2D;

    private static final Double DEFAULT_PRICE_CHANGE_PERCENT_14_D = 1D;
    private static final Double UPDATED_PRICE_CHANGE_PERCENT_14_D = 2D;

    private static final Integer DEFAULT_TWITTER_FOLLOWERS = 1;
    private static final Integer UPDATED_TWITTER_FOLLOWERS = 2;

    private static final Integer DEFAULT_TELEGRAM_USERS = 1;
    private static final Integer UPDATED_TELEGRAM_USERS = 2;

    @Autowired
    private CoinRepository coinRepository;

    @Autowired
    private CoinService coinService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoinMockMvc;

    private Coin coin;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coin createEntity(EntityManager em) {
        Coin coin = new Coin()
            .coinId(DEFAULT_COIN_ID)
            .symbol(DEFAULT_SYMBOL)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .homepage(DEFAULT_HOMEPAGE)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .coingeckoRank(DEFAULT_COINGECKO_RANK)
            .coingeckoScore(DEFAULT_COINGECKO_SCORE)
            .developperScore(DEFAULT_DEVELOPPER_SCORE)
            .communityScore(DEFAULT_COMMUNITY_SCORE)
            .liquidityScore(DEFAULT_LIQUIDITY_SCORE)
            .lastUpdated(DEFAULT_LAST_UPDATED)
            .marketCap(DEFAULT_MARKET_CAP)
            .marketCapRank(DEFAULT_MARKET_CAP_RANK)
            .high24HUSD(DEFAULT_HIGH_24_HUSD)
            .low24HUSD(DEFAULT_LOW_24_HUSD)
            .priceChangePercent1H(DEFAULT_PRICE_CHANGE_PERCENT_1_H)
            .priceChangePercent24H(DEFAULT_PRICE_CHANGE_PERCENT_24_H)
            .priceChangePercent7D(DEFAULT_PRICE_CHANGE_PERCENT_7_D)
            .priceChangePercent14D(DEFAULT_PRICE_CHANGE_PERCENT_14_D)
            .twitterFollowers(DEFAULT_TWITTER_FOLLOWERS)
            .telegramUsers(DEFAULT_TELEGRAM_USERS);
        return coin;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coin createUpdatedEntity(EntityManager em) {
        Coin coin = new Coin()
            .coinId(UPDATED_COIN_ID)
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .homepage(UPDATED_HOMEPAGE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .coingeckoRank(UPDATED_COINGECKO_RANK)
            .coingeckoScore(UPDATED_COINGECKO_SCORE)
            .developperScore(UPDATED_DEVELOPPER_SCORE)
            .communityScore(UPDATED_COMMUNITY_SCORE)
            .liquidityScore(UPDATED_LIQUIDITY_SCORE)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .marketCap(UPDATED_MARKET_CAP)
            .marketCapRank(UPDATED_MARKET_CAP_RANK)
            .high24HUSD(UPDATED_HIGH_24_HUSD)
            .low24HUSD(UPDATED_LOW_24_HUSD)
            .priceChangePercent1H(UPDATED_PRICE_CHANGE_PERCENT_1_H)
            .priceChangePercent24H(UPDATED_PRICE_CHANGE_PERCENT_24_H)
            .priceChangePercent7D(UPDATED_PRICE_CHANGE_PERCENT_7_D)
            .priceChangePercent14D(UPDATED_PRICE_CHANGE_PERCENT_14_D)
            .twitterFollowers(UPDATED_TWITTER_FOLLOWERS)
            .telegramUsers(UPDATED_TELEGRAM_USERS);
        return coin;
    }

    @BeforeEach
    public void initTest() {
        coin = createEntity(em);
    }

    @Test
    @Transactional
    public void createCoin() throws Exception {
        int databaseSizeBeforeCreate = coinRepository.findAll().size();
        // Create the Coin
        restCoinMockMvc
            .perform(post("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isCreated());

        // Validate the Coin in the database
        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeCreate + 1);
        Coin testCoin = coinList.get(coinList.size() - 1);
        assertThat(testCoin.getCoinId()).isEqualTo(DEFAULT_COIN_ID);
        assertThat(testCoin.getSymbol()).isEqualTo(DEFAULT_SYMBOL);
        assertThat(testCoin.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCoin.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCoin.getHomepage()).isEqualTo(DEFAULT_HOMEPAGE);
        assertThat(testCoin.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testCoin.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testCoin.getCoingeckoRank()).isEqualTo(DEFAULT_COINGECKO_RANK);
        assertThat(testCoin.getCoingeckoScore()).isEqualTo(DEFAULT_COINGECKO_SCORE);
        assertThat(testCoin.getDevelopperScore()).isEqualTo(DEFAULT_DEVELOPPER_SCORE);
        assertThat(testCoin.getCommunityScore()).isEqualTo(DEFAULT_COMMUNITY_SCORE);
        assertThat(testCoin.getLiquidityScore()).isEqualTo(DEFAULT_LIQUIDITY_SCORE);
        assertThat(testCoin.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
        assertThat(testCoin.getMarketCap()).isEqualTo(DEFAULT_MARKET_CAP);
        assertThat(testCoin.getMarketCapRank()).isEqualTo(DEFAULT_MARKET_CAP_RANK);
        assertThat(testCoin.getHigh24HUSD()).isEqualTo(DEFAULT_HIGH_24_HUSD);
        assertThat(testCoin.getLow24HUSD()).isEqualTo(DEFAULT_LOW_24_HUSD);
        assertThat(testCoin.getPriceChangePercent1H()).isEqualTo(DEFAULT_PRICE_CHANGE_PERCENT_1_H);
        assertThat(testCoin.getPriceChangePercent24H()).isEqualTo(DEFAULT_PRICE_CHANGE_PERCENT_24_H);
        assertThat(testCoin.getPriceChangePercent7D()).isEqualTo(DEFAULT_PRICE_CHANGE_PERCENT_7_D);
        assertThat(testCoin.getPriceChangePercent14D()).isEqualTo(DEFAULT_PRICE_CHANGE_PERCENT_14_D);
        assertThat(testCoin.getTwitterFollowers()).isEqualTo(DEFAULT_TWITTER_FOLLOWERS);
        assertThat(testCoin.getTelegramUsers()).isEqualTo(DEFAULT_TELEGRAM_USERS);
    }

    @Test
    @Transactional
    public void createCoinWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = coinRepository.findAll().size();

        // Create the Coin with an existing ID
        coin.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoinMockMvc
            .perform(post("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isBadRequest());

        // Validate the Coin in the database
        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCoinIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = coinRepository.findAll().size();
        // set the field null
        coin.setCoinId(null);

        // Create the Coin, which fails.

        restCoinMockMvc
            .perform(post("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isBadRequest());

        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSymbolIsRequired() throws Exception {
        int databaseSizeBeforeTest = coinRepository.findAll().size();
        // set the field null
        coin.setSymbol(null);

        // Create the Coin, which fails.

        restCoinMockMvc
            .perform(post("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isBadRequest());

        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = coinRepository.findAll().size();
        // set the field null
        coin.setName(null);

        // Create the Coin, which fails.

        restCoinMockMvc
            .perform(post("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isBadRequest());

        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCoins() throws Exception {
        // Initialize the database
        coinRepository.saveAndFlush(coin);

        // Get all the coinList
        restCoinMockMvc
            .perform(get("/api/coins?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coin.getId().intValue())))
            .andExpect(jsonPath("$.[*].coinId").value(hasItem(DEFAULT_COIN_ID)))
            .andExpect(jsonPath("$.[*].symbol").value(hasItem(DEFAULT_SYMBOL)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].homepage").value(hasItem(DEFAULT_HOMEPAGE)))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].coingeckoRank").value(hasItem(DEFAULT_COINGECKO_RANK)))
            .andExpect(jsonPath("$.[*].coingeckoScore").value(hasItem(DEFAULT_COINGECKO_SCORE.doubleValue())))
            .andExpect(jsonPath("$.[*].developperScore").value(hasItem(DEFAULT_DEVELOPPER_SCORE.doubleValue())))
            .andExpect(jsonPath("$.[*].communityScore").value(hasItem(DEFAULT_COMMUNITY_SCORE.doubleValue())))
            .andExpect(jsonPath("$.[*].liquidityScore").value(hasItem(DEFAULT_LIQUIDITY_SCORE.doubleValue())))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(DEFAULT_LAST_UPDATED.toString())))
            .andExpect(jsonPath("$.[*].marketCap").value(hasItem(DEFAULT_MARKET_CAP)))
            .andExpect(jsonPath("$.[*].marketCapRank").value(hasItem(DEFAULT_MARKET_CAP_RANK)))
            .andExpect(jsonPath("$.[*].high24HUSD").value(hasItem(DEFAULT_HIGH_24_HUSD.doubleValue())))
            .andExpect(jsonPath("$.[*].low24HUSD").value(hasItem(DEFAULT_LOW_24_HUSD.doubleValue())))
            .andExpect(jsonPath("$.[*].priceChangePercent1H").value(hasItem(DEFAULT_PRICE_CHANGE_PERCENT_1_H.doubleValue())))
            .andExpect(jsonPath("$.[*].priceChangePercent24H").value(hasItem(DEFAULT_PRICE_CHANGE_PERCENT_24_H.doubleValue())))
            .andExpect(jsonPath("$.[*].priceChangePercent7D").value(hasItem(DEFAULT_PRICE_CHANGE_PERCENT_7_D.doubleValue())))
            .andExpect(jsonPath("$.[*].priceChangePercent14D").value(hasItem(DEFAULT_PRICE_CHANGE_PERCENT_14_D.doubleValue())))
            .andExpect(jsonPath("$.[*].twitterFollowers").value(hasItem(DEFAULT_TWITTER_FOLLOWERS)))
            .andExpect(jsonPath("$.[*].telegramUsers").value(hasItem(DEFAULT_TELEGRAM_USERS)));
    }

    @Test
    @Transactional
    public void getCoin() throws Exception {
        // Initialize the database
        coinRepository.saveAndFlush(coin);

        // Get the coin
        restCoinMockMvc
            .perform(get("/api/coins/{id}", coin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coin.getId().intValue()))
            .andExpect(jsonPath("$.coinId").value(DEFAULT_COIN_ID))
            .andExpect(jsonPath("$.symbol").value(DEFAULT_SYMBOL))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.homepage").value(DEFAULT_HOMEPAGE))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.coingeckoRank").value(DEFAULT_COINGECKO_RANK))
            .andExpect(jsonPath("$.coingeckoScore").value(DEFAULT_COINGECKO_SCORE.doubleValue()))
            .andExpect(jsonPath("$.developperScore").value(DEFAULT_DEVELOPPER_SCORE.doubleValue()))
            .andExpect(jsonPath("$.communityScore").value(DEFAULT_COMMUNITY_SCORE.doubleValue()))
            .andExpect(jsonPath("$.liquidityScore").value(DEFAULT_LIQUIDITY_SCORE.doubleValue()))
            .andExpect(jsonPath("$.lastUpdated").value(DEFAULT_LAST_UPDATED.toString()))
            .andExpect(jsonPath("$.marketCap").value(DEFAULT_MARKET_CAP))
            .andExpect(jsonPath("$.marketCapRank").value(DEFAULT_MARKET_CAP_RANK))
            .andExpect(jsonPath("$.high24HUSD").value(DEFAULT_HIGH_24_HUSD.doubleValue()))
            .andExpect(jsonPath("$.low24HUSD").value(DEFAULT_LOW_24_HUSD.doubleValue()))
            .andExpect(jsonPath("$.priceChangePercent1H").value(DEFAULT_PRICE_CHANGE_PERCENT_1_H.doubleValue()))
            .andExpect(jsonPath("$.priceChangePercent24H").value(DEFAULT_PRICE_CHANGE_PERCENT_24_H.doubleValue()))
            .andExpect(jsonPath("$.priceChangePercent7D").value(DEFAULT_PRICE_CHANGE_PERCENT_7_D.doubleValue()))
            .andExpect(jsonPath("$.priceChangePercent14D").value(DEFAULT_PRICE_CHANGE_PERCENT_14_D.doubleValue()))
            .andExpect(jsonPath("$.twitterFollowers").value(DEFAULT_TWITTER_FOLLOWERS))
            .andExpect(jsonPath("$.telegramUsers").value(DEFAULT_TELEGRAM_USERS));
    }

    @Test
    @Transactional
    public void getNonExistingCoin() throws Exception {
        // Get the coin
        restCoinMockMvc.perform(get("/api/coins/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCoin() throws Exception {
        // Initialize the database
        coinService.save(coin);

        int databaseSizeBeforeUpdate = coinRepository.findAll().size();

        // Update the coin
        Coin updatedCoin = coinRepository.findById(coin.getId()).get();
        // Disconnect from session so that the updates on updatedCoin are not directly saved in db
        em.detach(updatedCoin);
        updatedCoin
            .coinId(UPDATED_COIN_ID)
            .symbol(UPDATED_SYMBOL)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .homepage(UPDATED_HOMEPAGE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .coingeckoRank(UPDATED_COINGECKO_RANK)
            .coingeckoScore(UPDATED_COINGECKO_SCORE)
            .developperScore(UPDATED_DEVELOPPER_SCORE)
            .communityScore(UPDATED_COMMUNITY_SCORE)
            .liquidityScore(UPDATED_LIQUIDITY_SCORE)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .marketCap(UPDATED_MARKET_CAP)
            .marketCapRank(UPDATED_MARKET_CAP_RANK)
            .high24HUSD(UPDATED_HIGH_24_HUSD)
            .low24HUSD(UPDATED_LOW_24_HUSD)
            .priceChangePercent1H(UPDATED_PRICE_CHANGE_PERCENT_1_H)
            .priceChangePercent24H(UPDATED_PRICE_CHANGE_PERCENT_24_H)
            .priceChangePercent7D(UPDATED_PRICE_CHANGE_PERCENT_7_D)
            .priceChangePercent14D(UPDATED_PRICE_CHANGE_PERCENT_14_D)
            .twitterFollowers(UPDATED_TWITTER_FOLLOWERS)
            .telegramUsers(UPDATED_TELEGRAM_USERS);

        restCoinMockMvc
            .perform(put("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(updatedCoin)))
            .andExpect(status().isOk());

        // Validate the Coin in the database
        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeUpdate);
        Coin testCoin = coinList.get(coinList.size() - 1);
        assertThat(testCoin.getCoinId()).isEqualTo(UPDATED_COIN_ID);
        assertThat(testCoin.getSymbol()).isEqualTo(UPDATED_SYMBOL);
        assertThat(testCoin.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCoin.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCoin.getHomepage()).isEqualTo(UPDATED_HOMEPAGE);
        assertThat(testCoin.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testCoin.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testCoin.getCoingeckoRank()).isEqualTo(UPDATED_COINGECKO_RANK);
        assertThat(testCoin.getCoingeckoScore()).isEqualTo(UPDATED_COINGECKO_SCORE);
        assertThat(testCoin.getDevelopperScore()).isEqualTo(UPDATED_DEVELOPPER_SCORE);
        assertThat(testCoin.getCommunityScore()).isEqualTo(UPDATED_COMMUNITY_SCORE);
        assertThat(testCoin.getLiquidityScore()).isEqualTo(UPDATED_LIQUIDITY_SCORE);
        assertThat(testCoin.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
        assertThat(testCoin.getMarketCap()).isEqualTo(UPDATED_MARKET_CAP);
        assertThat(testCoin.getMarketCapRank()).isEqualTo(UPDATED_MARKET_CAP_RANK);
        assertThat(testCoin.getHigh24HUSD()).isEqualTo(UPDATED_HIGH_24_HUSD);
        assertThat(testCoin.getLow24HUSD()).isEqualTo(UPDATED_LOW_24_HUSD);
        assertThat(testCoin.getPriceChangePercent1H()).isEqualTo(UPDATED_PRICE_CHANGE_PERCENT_1_H);
        assertThat(testCoin.getPriceChangePercent24H()).isEqualTo(UPDATED_PRICE_CHANGE_PERCENT_24_H);
        assertThat(testCoin.getPriceChangePercent7D()).isEqualTo(UPDATED_PRICE_CHANGE_PERCENT_7_D);
        assertThat(testCoin.getPriceChangePercent14D()).isEqualTo(UPDATED_PRICE_CHANGE_PERCENT_14_D);
        assertThat(testCoin.getTwitterFollowers()).isEqualTo(UPDATED_TWITTER_FOLLOWERS);
        assertThat(testCoin.getTelegramUsers()).isEqualTo(UPDATED_TELEGRAM_USERS);
    }

    @Test
    @Transactional
    public void updateNonExistingCoin() throws Exception {
        int databaseSizeBeforeUpdate = coinRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoinMockMvc
            .perform(put("/api/coins").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coin)))
            .andExpect(status().isBadRequest());

        // Validate the Coin in the database
        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCoin() throws Exception {
        // Initialize the database
        coinService.save(coin);

        int databaseSizeBeforeDelete = coinRepository.findAll().size();

        // Delete the coin
        restCoinMockMvc
            .perform(delete("/api/coins/{id}", coin.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coin> coinList = coinRepository.findAll();
        assertThat(coinList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
