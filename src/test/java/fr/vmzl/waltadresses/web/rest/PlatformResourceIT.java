package fr.vmzl.waltadresses.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import fr.vmzl.waltadresses.WaltAdressesApp;
import fr.vmzl.waltadresses.domain.Platform;
import fr.vmzl.waltadresses.repository.PlatformRepository;
import fr.vmzl.waltadresses.service.PlatformService;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PlatformResource} REST controller.
 */
@SpringBootTest(classes = WaltAdressesApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PlatformResourceIT {
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACT_ADDRESS = "BBBBBBBBBB";

    @Autowired
    private PlatformRepository platformRepository;

    @Autowired
    private PlatformService platformService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPlatformMockMvc;

    private Platform platform;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Platform createEntity(EntityManager em) {
        Platform platform = new Platform().name(DEFAULT_NAME).contractAddress(DEFAULT_CONTRACT_ADDRESS);
        return platform;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Platform createUpdatedEntity(EntityManager em) {
        Platform platform = new Platform().name(UPDATED_NAME).contractAddress(UPDATED_CONTRACT_ADDRESS);
        return platform;
    }

    @BeforeEach
    public void initTest() {
        platform = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlatform() throws Exception {
        int databaseSizeBeforeCreate = platformRepository.findAll().size();
        // Create the Platform
        restPlatformMockMvc
            .perform(post("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(platform)))
            .andExpect(status().isCreated());

        // Validate the Platform in the database
        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeCreate + 1);
        Platform testPlatform = platformList.get(platformList.size() - 1);
        assertThat(testPlatform.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlatform.getContractAddress()).isEqualTo(DEFAULT_CONTRACT_ADDRESS);
    }

    @Test
    @Transactional
    public void createPlatformWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = platformRepository.findAll().size();

        // Create the Platform with an existing ID
        platform.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlatformMockMvc
            .perform(post("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(platform)))
            .andExpect(status().isBadRequest());

        // Validate the Platform in the database
        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = platformRepository.findAll().size();
        // set the field null
        platform.setName(null);

        // Create the Platform, which fails.

        restPlatformMockMvc
            .perform(post("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(platform)))
            .andExpect(status().isBadRequest());

        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContractAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = platformRepository.findAll().size();
        // set the field null
        platform.setContractAddress(null);

        // Create the Platform, which fails.

        restPlatformMockMvc
            .perform(post("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(platform)))
            .andExpect(status().isBadRequest());

        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPlatforms() throws Exception {
        // Initialize the database
        platformRepository.saveAndFlush(platform);

        // Get all the platformList
        restPlatformMockMvc
            .perform(get("/api/platforms?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(platform.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].contractAddress").value(hasItem(DEFAULT_CONTRACT_ADDRESS)));
    }

    @Test
    @Transactional
    public void getPlatform() throws Exception {
        // Initialize the database
        platformRepository.saveAndFlush(platform);

        // Get the platform
        restPlatformMockMvc
            .perform(get("/api/platforms/{id}", platform.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(platform.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.contractAddress").value(DEFAULT_CONTRACT_ADDRESS));
    }

    @Test
    @Transactional
    public void getNonExistingPlatform() throws Exception {
        // Get the platform
        restPlatformMockMvc.perform(get("/api/platforms/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlatform() throws Exception {
        // Initialize the database
        platformService.save(platform);

        int databaseSizeBeforeUpdate = platformRepository.findAll().size();

        // Update the platform
        Platform updatedPlatform = platformRepository.findById(platform.getId()).get();
        // Disconnect from session so that the updates on updatedPlatform are not directly saved in db
        em.detach(updatedPlatform);
        updatedPlatform.name(UPDATED_NAME).contractAddress(UPDATED_CONTRACT_ADDRESS);

        restPlatformMockMvc
            .perform(
                put("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(updatedPlatform))
            )
            .andExpect(status().isOk());

        // Validate the Platform in the database
        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeUpdate);
        Platform testPlatform = platformList.get(platformList.size() - 1);
        assertThat(testPlatform.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlatform.getContractAddress()).isEqualTo(UPDATED_CONTRACT_ADDRESS);
    }

    @Test
    @Transactional
    public void updateNonExistingPlatform() throws Exception {
        int databaseSizeBeforeUpdate = platformRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPlatformMockMvc
            .perform(put("/api/platforms").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(platform)))
            .andExpect(status().isBadRequest());

        // Validate the Platform in the database
        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePlatform() throws Exception {
        // Initialize the database
        platformService.save(platform);

        int databaseSizeBeforeDelete = platformRepository.findAll().size();

        // Delete the platform
        restPlatformMockMvc
            .perform(delete("/api/platforms/{id}", platform.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Platform> platformList = platformRepository.findAll();
        assertThat(platformList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
