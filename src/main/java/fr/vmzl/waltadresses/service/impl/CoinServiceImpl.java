package fr.vmzl.waltadresses.service.impl;

import fr.vmzl.waltadresses.domain.Coin;
import fr.vmzl.waltadresses.repository.CoinRepository;
import fr.vmzl.waltadresses.service.CoinService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Coin}.
 */
@Service
@Transactional
public class CoinServiceImpl implements CoinService {
    private final Logger log = LoggerFactory.getLogger(CoinServiceImpl.class);

    private final CoinRepository coinRepository;

    public CoinServiceImpl(CoinRepository coinRepository) {
        this.coinRepository = coinRepository;
    }

    @Override
    public Coin save(Coin coin) {
        log.debug("Request to save Coin : {}", coin);
        return coinRepository.save(coin);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Coin> findAll() {
        log.debug("Request to get all Coins");
        return coinRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Coin> findOne(Long id) {
        log.debug("Request to get Coin : {}", id);
        return coinRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Coin : {}", id);
        coinRepository.deleteById(id);
    }
}
