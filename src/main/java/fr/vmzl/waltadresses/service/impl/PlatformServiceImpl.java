package fr.vmzl.waltadresses.service.impl;

import fr.vmzl.waltadresses.domain.Platform;
import fr.vmzl.waltadresses.repository.PlatformRepository;
import fr.vmzl.waltadresses.service.PlatformService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Platform}.
 */
@Service
@Transactional
public class PlatformServiceImpl implements PlatformService {
    private final Logger log = LoggerFactory.getLogger(PlatformServiceImpl.class);

    private final PlatformRepository platformRepository;

    public PlatformServiceImpl(PlatformRepository platformRepository) {
        this.platformRepository = platformRepository;
    }

    @Override
    public Platform save(Platform platform) {
        log.debug("Request to save Platform : {}", platform);
        return platformRepository.save(platform);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Platform> findAll() {
        log.debug("Request to get all Platforms");
        return platformRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Platform> findOne(Long id) {
        log.debug("Request to get Platform : {}", id);
        return platformRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Platform : {}", id);
        platformRepository.deleteById(id);
    }
}
