package fr.vmzl.waltadresses.service;

import fr.vmzl.waltadresses.domain.Platform;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Platform}.
 */
public interface PlatformService {
    /**
     * Save a platform.
     *
     * @param platform the entity to save.
     * @return the persisted entity.
     */
    Platform save(Platform platform);

    /**
     * Get all the platforms.
     *
     * @return the list of entities.
     */
    List<Platform> findAll();

    /**
     * Get the "id" platform.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Platform> findOne(Long id);

    /**
     * Delete the "id" platform.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
