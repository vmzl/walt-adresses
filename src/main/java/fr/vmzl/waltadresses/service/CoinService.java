package fr.vmzl.waltadresses.service;

import fr.vmzl.waltadresses.domain.Coin;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link Coin}.
 */
public interface CoinService {
    /**
     * Save a coin.
     *
     * @param coin the entity to save.
     * @return the persisted entity.
     */
    Coin save(Coin coin);

    /**
     * Get all the coins.
     *
     * @return the list of entities.
     */
    List<Coin> findAll();

    /**
     * Get the "id" coin.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Coin> findOne(Long id);

    /**
     * Delete the "id" coin.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
