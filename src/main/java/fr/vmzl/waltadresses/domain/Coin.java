package fr.vmzl.waltadresses.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Coin.
 */
@Entity
@Table(name = "coin")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Coin implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "coin_id", nullable = false, unique = true)
    private String coinId;

    @NotNull
    @Column(name = "symbol", nullable = false)
    private String symbol;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "homepage")
    private String homepage;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "coingecko_rank")
    private Integer coingeckoRank;

    @Column(name = "coingecko_score")
    private Double coingeckoScore;

    @Column(name = "developper_score")
    private Double developperScore;

    @Column(name = "community_score")
    private Double communityScore;

    @Column(name = "liquidity_score")
    private Double liquidityScore;

    @Column(name = "last_updated")
    private LocalDate lastUpdated;

    @Column(name = "market_cap")
    private Integer marketCap;

    @Column(name = "market_cap_rank")
    private Integer marketCapRank;

    @Column(name = "high_24_husd")
    private Double high24HUSD;

    @Column(name = "low_24_husd")
    private Double low24HUSD;

    @Column(name = "price_change_percent_1_h")
    private Double priceChangePercent1H;

    @Column(name = "price_change_percent_24_h")
    private Double priceChangePercent24H;

    @Column(name = "price_change_percent_7_d")
    private Double priceChangePercent7D;

    @Column(name = "price_change_percent_14_d")
    private Double priceChangePercent14D;

    @Column(name = "twitter_followers")
    private Integer twitterFollowers;

    @Column(name = "telegram_users")
    private Integer telegramUsers;

    @OneToMany(mappedBy = "coin")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Platform> platforms = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoinId() {
        return coinId;
    }

    public Coin coinId(String coinId) {
        this.coinId = coinId;
        return this;
    }

    public void setCoinId(String coinId) {
        this.coinId = coinId;
    }

    public String getSymbol() {
        return symbol;
    }

    public Coin symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public Coin name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Coin description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHomepage() {
        return homepage;
    }

    public Coin homepage(String homepage) {
        this.homepage = homepage;
        return this;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public byte[] getImage() {
        return image;
    }

    public Coin image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Coin imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public Integer getCoingeckoRank() {
        return coingeckoRank;
    }

    public Coin coingeckoRank(Integer coingeckoRank) {
        this.coingeckoRank = coingeckoRank;
        return this;
    }

    public void setCoingeckoRank(Integer coingeckoRank) {
        this.coingeckoRank = coingeckoRank;
    }

    public Double getCoingeckoScore() {
        return coingeckoScore;
    }

    public Coin coingeckoScore(Double coingeckoScore) {
        this.coingeckoScore = coingeckoScore;
        return this;
    }

    public void setCoingeckoScore(Double coingeckoScore) {
        this.coingeckoScore = coingeckoScore;
    }

    public Double getDevelopperScore() {
        return developperScore;
    }

    public Coin developperScore(Double developperScore) {
        this.developperScore = developperScore;
        return this;
    }

    public void setDevelopperScore(Double developperScore) {
        this.developperScore = developperScore;
    }

    public Double getCommunityScore() {
        return communityScore;
    }

    public Coin communityScore(Double communityScore) {
        this.communityScore = communityScore;
        return this;
    }

    public void setCommunityScore(Double communityScore) {
        this.communityScore = communityScore;
    }

    public Double getLiquidityScore() {
        return liquidityScore;
    }

    public Coin liquidityScore(Double liquidityScore) {
        this.liquidityScore = liquidityScore;
        return this;
    }

    public void setLiquidityScore(Double liquidityScore) {
        this.liquidityScore = liquidityScore;
    }

    public LocalDate getLastUpdated() {
        return lastUpdated;
    }

    public Coin lastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public void setLastUpdated(LocalDate lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Integer getMarketCap() {
        return marketCap;
    }

    public Coin marketCap(Integer marketCap) {
        this.marketCap = marketCap;
        return this;
    }

    public void setMarketCap(Integer marketCap) {
        this.marketCap = marketCap;
    }

    public Integer getMarketCapRank() {
        return marketCapRank;
    }

    public Coin marketCapRank(Integer marketCapRank) {
        this.marketCapRank = marketCapRank;
        return this;
    }

    public void setMarketCapRank(Integer marketCapRank) {
        this.marketCapRank = marketCapRank;
    }

    public Double getHigh24HUSD() {
        return high24HUSD;
    }

    public Coin high24HUSD(Double high24HUSD) {
        this.high24HUSD = high24HUSD;
        return this;
    }

    public void setHigh24HUSD(Double high24HUSD) {
        this.high24HUSD = high24HUSD;
    }

    public Double getLow24HUSD() {
        return low24HUSD;
    }

    public Coin low24HUSD(Double low24HUSD) {
        this.low24HUSD = low24HUSD;
        return this;
    }

    public void setLow24HUSD(Double low24HUSD) {
        this.low24HUSD = low24HUSD;
    }

    public Double getPriceChangePercent1H() {
        return priceChangePercent1H;
    }

    public Coin priceChangePercent1H(Double priceChangePercent1H) {
        this.priceChangePercent1H = priceChangePercent1H;
        return this;
    }

    public void setPriceChangePercent1H(Double priceChangePercent1H) {
        this.priceChangePercent1H = priceChangePercent1H;
    }

    public Double getPriceChangePercent24H() {
        return priceChangePercent24H;
    }

    public Coin priceChangePercent24H(Double priceChangePercent24H) {
        this.priceChangePercent24H = priceChangePercent24H;
        return this;
    }

    public void setPriceChangePercent24H(Double priceChangePercent24H) {
        this.priceChangePercent24H = priceChangePercent24H;
    }

    public Double getPriceChangePercent7D() {
        return priceChangePercent7D;
    }

    public Coin priceChangePercent7D(Double priceChangePercent7D) {
        this.priceChangePercent7D = priceChangePercent7D;
        return this;
    }

    public void setPriceChangePercent7D(Double priceChangePercent7D) {
        this.priceChangePercent7D = priceChangePercent7D;
    }

    public Double getPriceChangePercent14D() {
        return priceChangePercent14D;
    }

    public Coin priceChangePercent14D(Double priceChangePercent14D) {
        this.priceChangePercent14D = priceChangePercent14D;
        return this;
    }

    public void setPriceChangePercent14D(Double priceChangePercent14D) {
        this.priceChangePercent14D = priceChangePercent14D;
    }

    public Integer getTwitterFollowers() {
        return twitterFollowers;
    }

    public Coin twitterFollowers(Integer twitterFollowers) {
        this.twitterFollowers = twitterFollowers;
        return this;
    }

    public void setTwitterFollowers(Integer twitterFollowers) {
        this.twitterFollowers = twitterFollowers;
    }

    public Integer getTelegramUsers() {
        return telegramUsers;
    }

    public Coin telegramUsers(Integer telegramUsers) {
        this.telegramUsers = telegramUsers;
        return this;
    }

    public void setTelegramUsers(Integer telegramUsers) {
        this.telegramUsers = telegramUsers;
    }

    public Set<Platform> getPlatforms() {
        return platforms;
    }

    public Coin platforms(Set<Platform> platforms) {
        this.platforms = platforms;
        return this;
    }

    public Coin addPlatforms(Platform platform) {
        this.platforms.add(platform);
        platform.setCoin(this);
        return this;
    }

    public Coin removePlatforms(Platform platform) {
        this.platforms.remove(platform);
        platform.setCoin(null);
        return this;
    }

    public void setPlatforms(Set<Platform> platforms) {
        this.platforms = platforms;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Coin)) {
            return false;
        }
        return id != null && id.equals(((Coin) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Coin{" +
            "id=" + getId() +
            ", coinId='" + getCoinId() + "'" +
            ", symbol='" + getSymbol() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", homepage='" + getHomepage() + "'" +
            ", image='" + getImage() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", coingeckoRank=" + getCoingeckoRank() +
            ", coingeckoScore=" + getCoingeckoScore() +
            ", developperScore=" + getDevelopperScore() +
            ", communityScore=" + getCommunityScore() +
            ", liquidityScore=" + getLiquidityScore() +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", marketCap=" + getMarketCap() +
            ", marketCapRank=" + getMarketCapRank() +
            ", high24HUSD=" + getHigh24HUSD() +
            ", low24HUSD=" + getLow24HUSD() +
            ", priceChangePercent1H=" + getPriceChangePercent1H() +
            ", priceChangePercent24H=" + getPriceChangePercent24H() +
            ", priceChangePercent7D=" + getPriceChangePercent7D() +
            ", priceChangePercent14D=" + getPriceChangePercent14D() +
            ", twitterFollowers=" + getTwitterFollowers() +
            ", telegramUsers=" + getTelegramUsers() +
            "}";
    }
}
