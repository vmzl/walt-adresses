/**
 * View Models used by Spring MVC REST controllers.
 */
package fr.vmzl.waltadresses.web.rest.vm;
